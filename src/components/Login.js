import react ,{Component} from 'react'
import getEvaluators from '../services/LoginService';
import { Redirect } from 'react-router'

//login component represent login page
class  Login extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            firstName:'',
            goToHome: false
        }
    }
    handleChange = (e) => {
        const { name, value } = e.target
        this.setState({ [name]: value })
    }
    componentDidMount() {    
    }
    
    //get Evaluators and checking of credentials
    getData = () => {
        getEvaluators().then(response => {
            for (var i = 0; i < response.data.length; i++){
                if(response.data[i].email === this.state.email && response.data[i].password === this.state.password ){
                    this.setState({goToHome: true, firstName: response.data[i].firstName })                 
                }
            }
            return <Redirect push to={`/home`} />
        })
    }
    
    handleSubmit = (e) =>{
        e.preventDefault()
        this.getData();
    }
    render(){
        if (this.state.goToHome) {
            return <Redirect push to={`/home`} />
          }
        return (
            <react.Fragment>
                <div className="auth option2">
                    <div className="auth_left">
                            <div className="card">
                                <div className="card-body">
                                    <div className="text-center">
                                    <a className="header-brand" href="index.html"><img className="logo" src="assets/images/logo.png" alt='logo' width={150} /></a>
                                        <div className="card-title mt-3">Connexion</div>
                                        <button type="button" className="btn btn-facebook"><i className="fa fa-facebook mr-2"></i>Facebook</button>
                                        <button type="button" className="btn btn-google"><i className="fa fa-google mr-2"></i>Google</button>
                                        <h6 className="mt-3 mb-3">ou</h6>
                                    </div>
                                        <div className="form-group">
                                            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Entrez votre email" name='email' value={this.state.email} onChange={this.handleChange}/>
                                        </div>
                                        <div className="form-group">
                                            <label className="form-label"><a href="forgot-password.html" className="float-right small">Mot de passe oublié</a></label>
                                            <input type="password" className="form-control" id="exampleInputPassword1" name='password' placeholder="Mot de passe" value={this.state.password} onChange={this.handleChange}/>
                                        </div>
                                        <div className="form-group">
                                                <label className="custom-control custom-checkbox">
                                                <input type="checkbox" className="custom-control-input" />
                                                <span className="custom-control-label">se souvenir de moi</span>
                                                </label>
                                        </div>
                                        <div className="text-center">
                                                <button className="btn btn-primary" onClick= {this.handleSubmit}>Se connecter</button>
                                                <div className="text-muted mt-4">Pas de compte?<a href="register.html">S'enregistrer</a></div>
                                        </div>
                                </div>
                            </div>        
                    </div>
                </div>
            </react.Fragment>
        )
    }
}
export default Login
