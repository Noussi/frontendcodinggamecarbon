import React from 'react'

//Footer of App
const Footer= () =>{
    return(
        <footer className="footer">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-6 col-sm-12">
                    Copyright © Carbon IT 2022.
                </div>
            </div>
            </div>
        </footer>
    )
} 
export default Footer