import React from 'react'

//this component is called at the end of the evalution to say thank you 
const End =() => {
  
  //redirect to home when all is ok
  const goToHome = () =>{
    window.location = "/"
  }
  return (
  <div class="auth option2">
    <div class="auth_left">
      <div class="card">
        <div class="card-body text-center">
          <a className="header-brand" href="index.html"><img className="logo" src="assets/images/merci1.jpg" alt='logo' width={150} /></a>
          <h1 class="h3 mb-3">Merci d'avoir passé ce test !</h1>
          <p class="h6 text-muted font-weight-normal mb-3">Le recruteur reviendra vers vous rapidement pour vous donner les résultats</p>
          <button class="btn btn-primary" onClick={goToHome}>OK</button>
        </div>
      </div>        
    </div>
  </div>
  )
}

export default End;
