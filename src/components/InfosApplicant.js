import react, {Component} from 'react'
import Sidebar from './Sidebar'
import Header from './Header'
import getEvaluators from '../services/LoginService'
import getCampaigns from '../services/CampaignsService'
import { Redirect } from 'react-router'
import axios from "axios"

//component load when we want to select informations from applicant in oder to send email
class InfosApplicant extends Component {

    state = {
    evaluators: [],
    campaigns:[],
    goToEmailPage:false,
    applicantName:'',
    applicantEmail:'',
    applicantCreatedId:0
    }
    handleChange = (e) => {
        const { name, value } = e.target
        this.setState({ [name]: value })
    }
    componentDidMount() {  
      this.getData()  
    }
    //to get Evaluators and campaigns
    getData(){
        getEvaluators().then(response => {
            this.setState({evaluators: response.data})
        });
        getCampaigns().then(response =>{
            this.setState({campaigns: response.data})
        })  
    }
    // redirect to email view page
    goToEmailViewPage = () =>{
        this.setState({goToEmailPage:true})
    }
    // add applicant into database
    addApplicant = () =>{
        axios.post("http://localhost:9090/applicants", {firstName:this.state.applicantName, lastName:this.state.applicantName,email:this.state.applicantEmail,training:"Software engineering"}).then((response) => {
            if(response){
                this.setState({applicantCreatedId: response.data.id})
            }
            if(this.state.applicantCreatedId){
                this.goToEmailViewPage()
            }
        })
    }
   render(){
    if (this.state.goToEmailPage) {
        return <Redirect to={{
            pathname:'/emailview',
            state:{
                applicantEmail:this.state.applicantEmail,
                applicantName:this.state.applicantName,
                applicantCreatedId:this.state.applicantCreatedId
            }
        }} />
    }
     return (
        <react.Fragment>
            <div className="page-loader-wrapper">
                <div className="loader">
                </div>
            </div>
            <div id="main_content">
                {/* Start page sidebar */}
                <Sidebar/>
                <div className="page">
                    {/* Start Page header */}
                    <Header/>
                    <div className="section-body mt-4">
                        <div className="container-fluid">
                            <div className="tab-content">
                            <div className="tab-pane active show" id="Company_Settings">
                                <div className="card">
                                <div className="card-header">
                                    <h3 className="card-title">Entrer le nom et l'email du candidat</h3>
                                </div>
                                <div className="card-body">
                                    <form>
                                    <div className="row">
                                        <div className="col-md-4 col-sm-12">
                                        <div className="form-group">
                                            <label>Nom</label>
                                            <input className="form-control" type="text"  name="applicantName" value={this.state.applicantName} onChange={this.handleChange} />
                                        </div>
                                        </div>
                                        <div className="col-md-4 col-sm-12">
                                        <div className="form-group">
                                            <label>Email<span className="text-danger">*</span></label>
                                            <input className="form-control" type="text" name="applicantEmail" value={this.state.applicantEmail} onChange={this.handleChange}/>
                                        </div>
                                        </div>
                                        <div className="col-md-4 col-sm-12">
                                        <div className="form-group">
                                            <label>Campagnes <span className="text-danger">*</span></label>
                                            <select class="form-control">
                                                <option value="">-------</option>
                                                {
                                                    this.state.campaigns.map(
                                                    (campaign) =>
                                                        <option>{campaign.name}</option>  
                                                    )
                                                }
                                            </select>
                                        </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-9">
                                        <div className="form-group">
                                            <label>Sélectionnez les differents utilisateurs qui recevrons le rapport du candidat<span className="text-danger">*</span></label>
                                            <select class="form-control">
                                                <option value="">-------</option>
                                                {
                                                    this.state.evaluators.map(
                                                    (evaluator) =>
                                                        <option>{evaluator.firstName} {evaluator.lastName}</option>  
                                                    )
                                                }
                                            </select>
                                        </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-12 text-right m-t-20">
                                        <button type="button" className="btn btn-primary" onClick={this.addApplicant}>Suivant</button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                                </div>                        
                            </div>
                            </div>
                        </div>
                    </div>
                     {/* Start main footer */}
                     <div className="section-body">
                     </div>
                </div>
            </div>
      </react.Fragment>
    )}
}

export default InfosApplicant
