import React, {useState, useEffect} from 'react'
import CampaignInfos from './CampaignInfos'
import QuestionByTechInfos from './QuestionByTechInfos'
import TechnologiesInfos from './TechnologiesInfos'
import Header from './Header'
import Sidebar from './Sidebar'
import getTechnologies from '../services/TechnologiesService'
import getQuestions from '../services/QuestionsService'
import axios from "axios"
import Swal from 'sweetalert2'

//this component is a Form that render questions dynamically
const Form = props => { 
    //state for managing next and previous page
    const [page, setPage] = useState(0);
    //state for campaing name
    const [campaignName, setCampaignName] = useState('')
    // state for level of evaluation
    const [level,setLevel] = useState(1)
    //set for main form
    const [formData, setFormData] = useState({
        technologies: [],
        questions:[],
        technologies_selected:[],
        goToHome: false,
    });
      
    useEffect(() => {
        getData()
    },[]);
    const FormTitles = ["Technologies Info", "Campaign Infos", "Question By Technology"]
     
    //this function is use to display component according to page set
    const PageDisplay = () => {
        if(page === 0){
          return <TechnologiesInfos formData={formData} setFormData={setFormData} setLevel={setLevel} level={level}/>

        }else if(page === 1){
            return <CampaignInfos campaignName={campaignName} setCampaignName={setCampaignName}/>
        }else {
            return <QuestionByTechInfos formData={formData} setFormData={setFormData} handleChangeSelected={handleChangeSelected} level={level}/>
        }
    }
   
    //to get Questions and technologies to database
    const getData = () => {
        getQuestions().then(response => {
            setFormData({questions: response.data})
        });
        getTechnologies().then(response => {
            setFormData({technologies: response.data})
        });
    }
    // add a campaign to database
    const addCampaign = () =>{
        axios.post("http://localhost:9090/campaigns", {name:campaignName, level:{id:level}}).then((response) => {
            if(response){
             success()
            }
        })
    }
    //to save the technologies selected
    const handleChangeSelected = (e) => {
        let value = Array.from(e.target.selectedOptions, option => option.value);
        setFormData({technologies_selected: value})
    }
    // to display a pop up when all is good
   const success = () =>{
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-success',
              cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
          })

        swalWithBootstrapButtons.fire({
            position: 'center',
            icon: 'success',
            title: 'campagne créée',
            confirmButtonText: 'OK'
        }).then((result) => {
            if (result.isConfirmed) {
                props.history.push("home")
            };
        })
    }
        return (
            <div>
                <div className="page-loader-wrapper">
                    <div className="loader">
                    </div>
                </div>
                <div id="main_content">
                    <Sidebar/>
                    <div className="page">
                        <Header/>
                        <div className="section-body mt-4">
                            <div className="container-fluid">
                                <div className="row clearfix">
                                    <div className="col-lg-12">
                                        <div className="form">
                                            <div className="progressbar">
                                              <div
                                                style={{width: page === 0 ? "33.3%" : page === 1 ? "66.6%" : "100%"}}>
                                              </div>
                                            </div>
                                            <div className="form-container">
                                                <div className='header'>
                                                    <h1>{FormTitles[page]}</h1>
                                                </div>
                                                <div className='body'>{PageDisplay()}</div>
                                                <div className="tab-content"></div>
                                                <div className='footer'>
                                                    <button className="btn btn-primary"
                                                        disabled = {page === 0}
                                                        onClick={() => {
                                                        setPage((currPage) => currPage - 1);
                                                        }}
                                                        >Précédent
                                                    </button>
                                                    <button className="btn btn-primary"
                                                        disabled = {page === FormTitles.length -1}
                                                        onClick={() => {
                                                        setPage((currPage) => currPage + 1);
                                                        }}
                                                        >Suivant
                                                    </button>
                                                    { page === 2 &&
                                                    <button  className="btn btn-primary" onClick={ () => addCampaign()}
                                                    >Valider</button>}
                                                </div>
                                            </div>
                                        </div>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
           </div>
        )
    }

export default Form
