import React from 'react'

// Header of App
const Header = () => {

  return (
    <div className="section-body" id="page_top">
      <div className="container-fluid">
        <div className="page-header">
          <div className="left">                        
            <div className="dropdown d-flex">
              <a className="header-brand" href="index.html"><img className="logo" src="assets/images/logo2.png" alt='logo' width={170} /></a>
            </div>
          </div>
          <div className="right">
            <ul className="nav nav-pills">
            </ul>
            <div className="notification d-flex">
              <div className="dropdown d-flex">
                <a className="nav-link icon d-none d-md-flex btn btn-default btn-icon ml-1" data-toggle="dropdown" alt="image" href="image.html"><i className="fa fa-language" /></a>
                <div className="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                 <a className="dropdown-item" href="page-search.html"><img className="w20 mr-2" src="../assets/images/flags/us.svg" alt="image9" />English</a>
                <div className="dropdown-divider" />
                 <a className="dropdown-item" href="page-search.html"><img className="w20 mr-2" src="../assets/images/flags/bl.svg" alt="imu96" />France</a>
                </div>
              </div>
              <div className="dropdown d-flex">
                <a className="nav-link icon d-none d-md-flex btn btn-default btn-icon ml-1" data-toggle="dropdown" href="page-search.html"><i className="fa fa-envelope" /><span className="badge badge-success nav-unread" /></a>
              </div>
              <div className="dropdown d-flex">
                <div className="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                  <ul className="list-unstyled feeds_widget">
                    <li>
                      <div className="feeds-left">
                        <span className="avatar avatar-green"><i className="fa fa-user" /></span>
                      </div>
                      <div className="feeds-body ml-3">
                        <p className="text-muted mb-0"><strong className="text-green font-weight-bold"> 1</strong> Rapport disponible</p>
                        <a href='http://localhost:3000/reportEval6321893'>Consulter</a>
                      </div>
                    </li>
                  </ul>
                  <div className="dropdown-divider" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
   </div>
  )
}

export default Header
