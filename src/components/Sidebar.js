import React from 'react'

//sidebar of App
const Sidebar = () => {
  return (
    <React.Fragment>
    <div id="header_top" className="header_top">
      <div className="container">
        <div className="hleft show active">
          <div className="dropdown">
            <a href="page-search.html" className="nav-link icon menu_toggle"><i className="fe fe-align-center" /></a>
            <a href="page-search.html" className="nav-link icon"><i className="fe fe-search" data-toggle="tooltip" data-placement="right" title="Search..." /></a>
            <a href="app-email.html" className="nav-link icon app_inbox"><i className="fe fe-inbox" data-toggle="tooltip" data-placement="right" title="Inbox" /></a>
            <a href="app-filemanager.html" className="nav-link icon app_file xs-hide"><i className="fe fe-folder" data-toggle="tooltip" data-placement="right" title="File Manager" /></a>
            <a href="app-social.html" className="nav-link icon xs-hide"><i className="fe fe-share-2" data-toggle="tooltip" data-placement="right" title="Social Media" /></a>
            <a href="jpage-search.html" className="nav-link icon theme_btn"><i className="fe fe-feather" /></a>
            <a href="page-search.html" className="nav-link icon settingbar"><i className="fe fe-settings" /></a>
          </div>
        </div>
        <div className="hright">
          <a href="page-search.html" className="nav-link icon right_tab"><i className="fe fe-align-right" /></a>
          <a href="login.html" className="nav-link icon settingbar"><i className="fe fe-power" /></a>                
        </div>
      </div>
    </div>
    <div id="left-sidebar" className="sidebar">
    {/* <h5 className="brand-name">Carbon IT<a href='page-search.html' className="menu_option float-right"></a></h5>
      <ul className="nav nav-tabs">
        <li className="nav-item"><a className="nav-link" href="menu-uni.html"></a></li>
        <li className="nav-item"><a className="nav-link active" href="menu-admin.html"></a></li>
      </ul>*/}
      <div className="tab-content mt-3">
        
        <div className="tab-pane fade show active" id="menu-admin" role="tabpanel">
          <nav className="sidebar-nav">
          </nav>
        </div>
      </div>
    </div>
    </React.Fragment>
  )
}

export default Sidebar
