import React from 'react'
import {Multiselect} from 'multiselect-react-dropdown'

//this component is loaded to display technology in order to create campaigns
function TechnologiesInfos({formData,setFormData,level, setLevel}) {

    function handleChangeSelected(e){
        let value = Array.from(e.target.selectedOptions, option => option.value);
        setFormData({technologies_selected: value})
    }

  return (
    <><div className="tab-pane fade show active" role="tabpanel">
          <div className="card-body">
              <form>
                  <div className="row">
                      <div className="col-md-4 col-sm-12">
                          <div className="form-group">
                              <label>Sélectionnez la technologies <span className="text-danger">*</span></label>
                               <Multiselect options={formData.technologies} displayValue="name" onChange={event => handleChangeSelected(event)} />
                          </div>
                      </div>
                  </div>
                  <div className="row">
                      <div className="col-sm-9">
                          <div className="form-group">
                              <label>Sélectionnez le niveau d'expérience<span className="text-danger">*</span></label>
                              <div className="custom-controls-stacked">
                                  <label className="custom-control custom-radio custom-control-inline">
                                       <input type="radio" className="custom-control-input" name={level} value={level} onChange={event => setLevel({level: event.target.value})} defaultChecked/>
                                      <span className="custom-control-label">Débutant</span>
                                  </label>
                                  <label className="custom-control custom-radio custom-control-inline">
                                      <input type="radio" className="custom-control-input"  name={level}  onChange={event => setLevel({level: event.target.value})} />
                                      <span className="custom-control-label">Intermédiaire</span>
                                  </label>
                                  <label className="custom-control custom-radio custom-control-inline">
                                      <input type="radio" className="custom-control-input" name={level} onChange={event => setLevel({level: event.target.value})} />
                                      <span className="custom-control-label">Avancé</span>
                                  </label>
                              </div>
                          </div>
                      </div>
                  </div>
              </form>
          </div>
      </div>
    </>
  )
}

export default TechnologiesInfos