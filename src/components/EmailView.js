import React, {Component} from 'react'
import Sidebar from './Sidebar'
import Header from './Header'
import react from 'react'
import emailjs from 'emailjs-com'
import Swal from 'sweetalert2'
import 'animate.css'
import axios from 'axios'
import { Redirect } from 'react-router'

//the use to display email sending interface
class EmailView extends Component{
    
    constructor(props){
        super(props)
        this.state = {
            email: this.props.location.state.applicantEmail,
            message: '',
            subject:'évaluation technique',
            link_eval:"",
            first_name:this.props.location.state.applicantName,
            applicantCreatedId:this.props.location.state.applicantCreatedId,
            id: 0
        }
    }
  
    handleChange = (e) => {
        const { name, value } = e.target
        this.setState({ [name]: value })
    }
    //function to send email to an applicant
    handleSubmit =(e) =>{
      e.preventDefault();
      emailjs.sendForm('service_hng3g42', 'template_nhhf9ld',e.target, 'u39pWyrhb5b-e9S88')
          .then((result) => {
              this.success();
              this.addEvaluation();
          }, (error) => {
        });
        e.target.reset();
    }
     //function to add evaluation to database
    addEvaluation = () =>{
        axios.post("http://localhost:9090/evaluations", {
            applicant:{id:this.state.applicantCreatedId},
            dateOfEvaluation:'0000-00-00',
            durationOfEvaluation:"00:00:00",
            durationOfApplicant:"00:00:00",
            score:"00",
            spring:"00",
            status:"Envoyée",
            color:"blue"
        }).then((response) => {
          })
    }
    //for displaying Pop up when email is sent 
    success(){
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-success',
              cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
          })

        swalWithBootstrapButtons.fire({
            title: 'l\'email a bien été envoyé!',
            showClass: {
              popup: 'animate__animated animate__fadeInDown'
            },
            hideClass: {
              popup: 'animate__animated animate__fadeOutUp'
            }
        }).then((result) => {
            if (result.isConfirmed) {
             this.setState({goToHome:true})
            }
        }) 
    }
    render(){
        if (this.state.goToHome) {
            return <Redirect push to={`/home`} />
        }
      return (
        <react.Fragment>
            <div className="page-loader-wrapper">
                <div className="loader">
                </div>
            </div>
            <div id="main_content">
                {/* Start page sidebar */}
                <Sidebar/>
                <div className="page">
                    {/* Start Page header */}
                    <Header/>
                    <div className="section-body mt-4"></div>
                    <div className="card-body detail">
                        <div className="section-body mt-4">
                            <div className="container-fluid">
                                <div className="row row-deck">
                                    <div className="col-12">
                                        <div className="card">
                                            <div className="card-body mail_compose">
                                            <form onSubmit={this.handleSubmit}>
                                                    <div className="form-group">
                                                    <input type="text" className="form-control" placeholder="A" name="email" value={this.state.email} onChange={this.handleChange}/>
                                                    </div>
                                                    <div className="form-group">
                                                    <input type="text" className="form-control" placeholder="" name="subject" value={this.state.subject} onChange={this.handleChange} />
                                                    </div>
                                                <div className="summernote" name="message" value={this.state.message} onChange={this.handleChange}>
                                                    <input type="hidden" name='link_eval' value={this.state.link_eval} onChange={this.handleChange}/>
                                                    <input type="hidden" name='first_name' value={this.state.first_name} onChange={this.handleChange}/>
                                                    <p>Bonjour {this.state.first_name} , </p>
                                                    <p>Votre candidature a retenu notre attention.</p>
                                                    <p>Dans le cadre de notre processus de recrutement, nous avons le plaisir de vous inviter à passer une évaluation technique. Vous pourrez choisir le moment le plus approprié pour vous pour passer ce test.</p>
                                                    <p>Quand vous serez prêt(e), cliquez sur le lien ci-dessous pour accéder à la page d'accueil de votre session: <a href='http://localhost:3000/evaluation/4377765'>lien_évaluation</a></p>
                                                    <p>Bonne chance !</p>
                                                    <p>Cordialement ,</p>
                                                    <p>L'équipe Carbon IT</p>
                                                    <br/>
                                                    <p> <a className="header-brand" href="index.html"><img className="logo" src="assets/images/logo2.png" alt='logo' width={140} /></a></p>
                                                </div>
                                                <div className="mt-4 text-right">
                                                    <button type="submit" className="btn btn-success">Envoyer</button>
                                                </div>
                                            </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </react.Fragment>
    )}
} 

export default EmailView
