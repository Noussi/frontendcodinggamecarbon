import React from 'react'

// this is called when we ask informations about campaign in order to create evaluation
function CampaignInfos({campaignName, setCampaignName}){
  return (
    <>
        <div className="card-body">
            <form>
                <div className="row">
                    <div className="col-md-4 col-sm-12">
                        <div className="form-group">
                            <label>Nom de la campagne <span className="text-danger">*</span></label>
                            <input type="text" className="form-control" 
                            value={campaignName} onChange={(event) => setCampaignName(event.target.value)}/>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-9">
                        <div className="form-group">
                            <label>Envoyer un rapport simplifié au candidat à la fin du test<span className="text-danger">*</span></label>
                            <div className="custom-controls-stacked">
                                <label className="custom-control custom-radio custom-control-inline">
                                   <input type="radio" className="custom-control-input" name="example-inline-radios" defaultValue="option1" defaultChecked />
                                   <span className="custom-control-label">Oui</span>
                                </label>
                                <label className="custom-control custom-radio custom-control-inline">
                                  <input type="radio" className="custom-control-input" name="example-inline-radios" defaultValue="option2" />
                                  <span className="custom-control-label">Non</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
           </form>
       </div>
   </>
  )
}

export default CampaignInfos