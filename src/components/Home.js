import react,{ Component } from 'react'
import Sidebar from './Sidebar'
import Header from './Header'
import Footer from './Footer'
import getEvaluations from '../services/EvaluationService'
import React from 'react'
import { Redirect } from 'react-router'
import getCampaigns from '../services/CampaignsService'
import getEvaluationsByCampaign from '../services/EvaluationByCampaignService'

//the home page of application
class Home extends Component{
    // state to manage all the evaluations of database
    state = {
        evaluations: [],
        goToCampaignCreation: false,
        goToEvaluationCreation: false,
        campaigns: [],
        evaluationsByCampaign: [],
        campaign_selected: 0,
        status: ''
    }
    handleChange = (e) => {
        const { name, value } = e.target
        this.setState({ [name]: value })
    }
    componentDidMount() {  
      this.getData()  
      this.getCampaigns()
      this.getEvaluationsByCampaign(1)
    }
    //get all evaluations
    getData = () => {
        getEvaluations().then(response => {
            this.setState({evaluations: response.data})
        });
    }
    // get Evaluation by Campaign
    getEvaluationsByCampaign = (id) => {
       getEvaluationsByCampaign(id).then(response => {
           this.setState({evaluationsByCampaign: response.data})
       })
    }
    // get Campaigns
    getCampaigns = () => {
        getCampaigns().then(response =>{
            this.setState({campaigns: response.data})
        })
    }
    // update evaluation by campaign
    updateEvaluationsByCampaign = (id) => {
        getEvaluationsByCampaign(id).then(response => {
            this.setState({evaluationsByCampaign: response.data})
        })
    }
    // to go to campaign creation page after click on button
    goToCampaignPage = () => {
      this.setState({goToCampaignCreation:true})
    }
    // to go to creation evaluation and sending email page after click on button
    goToEvaluatioPage = () => {
        this.setState({goToEvaluationCreation:true})
    }
    //to save a campaing selected into state
    handleChangeSelected = (e) => {
        let value = Array.from(e.target.selectedOptions, option => option.value);
        this.setState({campaign_selected: value});
        if(value){
            this.updateEvaluationsByCampaign(value)
        }
    }
    render(){
        if (this.state.goToCampaignCreation) {
            return <Redirect push to={`/creation_of_campaign`} />
        }
        if (this.state.goToEvaluationCreation) {
            return <Redirect push to={`/envoieEvaluation`} />
        }
        return (
            <react.Fragment>
                <div>
                    <div className="page-loader-wrapper">
                        <div className="loader">
                        </div>
                    </div>
                    <div id="main_content">
                            {/* Start page sidebar */}
                            <Sidebar/>
                            <div className="page">
                            {/* Start Page header */}
                            <Header/>
                            {/* Start Page title and tab */}
                            <div className="section-body">
                                <div className="container-fluid">
                                    <div className="d-flex justify-content-between align-items-center ">
                                        <div className="header-action">
                                        <h1 className="page-title">Candidats</h1>
                                        <ol className="breadcrumb page-breadcrumb">
                                            <li className="breadcrumb-item"><a href="page-search.html">Carbon IT</a></li>
                                            <li className="breadcrumb-item active" aria-current="page">candidats</li>
                                        </ol>
                                        </div>
                                        <div>
                                        <form>
                                          <select className="form-control" name="campaign_selected" value={this.state.campaign_selected} onChange={this.handleChangeSelected}>
                                            {
                                                this.state.campaigns.map((campaign) =>
                                                    <option value={campaign.id}>{campaign.name}</option>  
                                                )
                                            }
                                          </select>
                                        </form>
                                        </div>
                                    </div>
                                    <br/>
                                    <div className="input-group">
                                        <div className="right">
                                            <button className="btn btn-primary" onClick={this.goToCampaignPage}>Créer une campagne </button>
                                        </div>
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                        <button className="btn btn-primary" onClick={this.goToEvaluatioPage}>Inviter un candidat </button>
                                    </div>
                                </div>
                            </div>
                            <div className="section-body mt-4">
                                <div className="container-fluid">
                                <div className="tab-content">
                                    <div className="tab-pane active" id="Fees-all">
                                    <div className="card">
                                        <div className="card-body">
                                        <div className="table-responsive">
                                            <table className="table table-hover text-nowrap js-basic-example dataTable table-striped table_custom border-style spacing5">
                                            <thead>
                                                <tr>
                                                <th>Candidats</th>
                                                <th>Dernière activité</th>
                                                <th>Score</th>
                                                <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                this.state.evaluationsByCampaign.map((evaluationByCampaign) =>
                                                    <tr>
                                                     <td>{evaluationByCampaign.applicant.email}</td>
                                                     <td>{evaluationByCampaign.dateOfEvaluation}</td>
                                                     <td>{evaluationByCampaign.score}</td>     
                                                     <td><span className={'tag tag-'+evaluationByCampaign.color}>{evaluationByCampaign.status}</span></td> 
                                                    </tr>
                                                )
                                            }
                                            </tbody>
                                            </table>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            {/* Start main footer */}
                            <div className="section-body">
                              <Footer/>
                            </div>
                       </div>    
                  </div>
                </div>
         </react.Fragment>
        )
    }
}

export default Home
