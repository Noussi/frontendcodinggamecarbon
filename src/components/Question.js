import React , {useEffect, useState} from 'react'
import Editor from "react-simple-code-editor";
import { highlight, languages } from "prismjs/components/prism-core";
import axios from 'axios'
import "prismjs/components/prism-clike";
import "prismjs/components/prism-javascript";
import "prismjs/themes/prism.css"; 

//Component question is load in App to display question d
const Question = ({data, onAnswerUpdate, numberOfQuestions, activeQuestion, onSetActiveQuestion,onSetstep}) => {
    
    // state for selected answer
    const [selected, setSelected] = useState('')
    // state to display error if user select anything
    const [error, setError] = useState('')
    const [code1, setCode1] = React.useState(
    `
    import React from "react";
    import ReactDOM from "react-dom";\n
    function App() {
      return (
        <h1>Go code</h1>
      );
    }

    ReactDOM.render(<App />, document.getElementById("root"));
    `,
    );
    
    const [code2, setCode2] = React.useState(
      `
@RequestMapping("itemQuestions")
public class ItemsQuestionsController {

    @GetMapping
    public getItemQuestions(){
        return Service.getItemQuestion();
    }
  }   
      `,
      );

    useEffect( () =>{
      if(data.type === 'text'){

        var x = document.form.answer;
        for(var i=0; i<x.length; i++){
          if(x[i].checked){
            x[i].checked = false
          }
        }

      }
    }, [data])

  const changeHandler = (e) =>{
    setSelected(e.target.value);
    if(error){
        setError('');
    }
  }
  //move to next question
  const nextClickHandler = (e) => {
    if(selected === '' && data.type === "text"  ){
        return setError('sélectionner une reponse')
    }
  // update selected answer to database
    onAnswerUpdate(prevState => [...prevState, {q: data.wording, a: selected}])

    const l = data.wording
    const s = selected
    //const d = data.response

    if(selected){
    axios.post("http://localhost:9090/responses", {wording:l, selected:s, response:data.response}).then((response) => {
      if(response){
      }})
    }
    setSelected('');
    if(activeQuestion < numberOfQuestions - 1){ 
        onSetActiveQuestion(activeQuestion + 1)
    }else
    {
      onSetstep(3)
    }
  }
  return (
  <div className="auth option2">
    <div className="auth_left">
      <div className="card">
        <div className="card-body">
          <div className="form-group">
            <p className="form-label" htmlFor="exampleInputEmail1">{data.wording}</p>
            <p>&nbsp;</p>
            {data.type === "text" &&
            <form name="form">
              {data.items.map((item,i) =>
                <ul class="list-group">
                  <label key={i}>
                  <li class="list-group-item list-group-item-secondary">
                    <input type="radio"  className="answer" name="answer" value={item} onChange={changeHandler} />
                    {item}
                  </li>
                  </label>
                </ul>
              )}
            </form>
            }
            {data.type === "code1" &&
            <Editor
            value={code1}
            onValueChange={(code1) => setCode1(code1)}
            highlight={(code1) => highlight(code1, languages.js)}
            padding={10}
            style={{
              fontFamily: '"Fira code", "Fira Mono", monospace',
              fontSize: 12,
            }}
            />}
            {data.type === "code2" &&
            <Editor
            value={code2}
            onValueChange={(code2) => setCode2(code2)}
            highlight={(code2) => highlight(code2, languages.js)}
            padding={10}
            style={{
              fontFamily: '"Fira code", "Fira Mono", monospace',
              fontSize: 12,
            }}
            />}
          </div>
          <div className="text-center">
            {error  && <ul class="list-group">
              <li class="list-group-item list-group-item-danger">
                <div className='has-text-danger'>{error}</div>
              </li>
            </ul>}
            <p></p>
            <button type="submit" className="btn btn-primary " onClick={nextClickHandler}>Question Suivante</button>
          </div>
          <div>
          </div>
        </div>        
      </div>
    </div>
  </div>
)}

export default Question