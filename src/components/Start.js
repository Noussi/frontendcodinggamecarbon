import React from 'react'

//this component is loaded in begining of evaluation
const Start = ({onTestStart}) => {
  return (
    <div className="auth option2">
      <div className="auth_left">
        <div className="card">
          <div className="card-body">
            <div className="text-center">
              <div className="card-title">GoCode</div>
            </div>
            <p>&nbsp;</p>
            <div className="form-group">
              <label className="form-label" htmlFor="exampleInputEmail1">Vous êtes sur le point de démarrer votre test</label>
              <label className="form-label" htmlFor="exampleInputEmail1"><p>En cliquant sur le button "Commencer", vous ne pourrez plus interrompre l'enchainement des questions.</p></label>
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="exampleInputEmail1">Veuillez noter que:</label>
              <ul>
                <li>Il est interdit de tricher</li>
                <li>Il est strictement interdit de divulguer les questions du test</li>
                <li>La politique de confidentialité de l'entreprise s'applique</li>
              </ul>
              Bonne Chance !
            </div>
            <div className="text-center">
              <button className="btn btn-primary btn-block" onClick={onTestStart}>Commencer</button>
            </div>
          </div>
        </div>        
      </div>
    </div>
  )
}

export default Start