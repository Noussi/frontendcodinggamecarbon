import React ,{useEffect, useState} from 'react'
import getQuestions from '../services/QuestionsService'

//component to display question by technology
function QuestionByTechInfos() {
    const [question, setQuestion] = useState({
        questions: []
    })

    const getData = () => {
        getQuestions().then(response => {
            setQuestion({questions: response.data})
        });
    }
    useEffect(() => {
        getData()
    },[]);
    return(
   <>
        <div className="card-body">
            <div className="container">
                <div className="col-sm-12">
                    <div className="tab-content">
                        <div className="tab-pane fade show active" id="list" role="tabpanel">
                            <div className="table-responsive" id="users">
                                <table className="table table-hover table-vcenter text-nowrap table_custom list">
                                    <tbody>
                                        {    
                                            question.questions.map((question, index) =>
                                               <tr key={index} className="" >
                                                  <td>
                                                    <div><a href="test.html">{question.technology.name}</a></div>
                                                    <div className="text-muted">1pt</div>
                                                  </td>
                                                  <td className="hidden-sm">
                                                    <div className="text-muted">{question.wording}</div>                                                
                                                  </td>
                                                </tr>
                                            )
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                   </div>   
                </div>
           </div>
       </div>
   </>                                
  )
}

export default QuestionByTechInfos