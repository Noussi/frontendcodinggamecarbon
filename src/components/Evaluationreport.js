import React,{Component} from 'react'
import Header from './Header'
import Sidebar from './Sidebar'
import getResp from '../services/RespService'
import ReactPlayer from 'react-player'


//this class is called to display a report of an evaluation
class Evaluationreport extends Component {

    state = {
        questions:[]
    }
    handleChange = (e) => {
        const { name, value } = e.target
        this.setState({ [name]: value })
    }
    //this is life cycle method called the first before others ones
    componentDidMount() {  
      this.getData()  
    }
    //to retrieve answers selected by an applicant
    getData = () => {
        getResp().then(response => {
            this.setState({questions: response.data})
        });
    }

    
    render(){
       
        return(
            <React.Fragment>
                <div>
    <div className="page-loader-wrapper">
        <div className="loader">
        </div>
    </div>
    <div id="main_content">
            {/* Start page sidebar */}
            <Sidebar/>
            <div className="page">
            {/* Start Page header */}
            <Header/>
            {/* Start Page title and tab */}
            <div className="section-body mt-4">
                <div className="container-fluid">
                    <div className="tab-content">
                        <div className="tab-pane active" id="TaskBoard-all">
                            <div className="row clearfix mt-2">
                                <div className="col-lg-3 col-md-6">
                                    <div className="card">
                                        <div className="card-body text-center">
                                            <h6>Pourcentage</h6>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-6">
                                </div>
                                <div className="col-lg-3 col-md-6">
                                    <div className="card">
                                        <div className="card-body text-center">
                                            <h6>p</h6>
                                            <input type="text" className="knob" defaultValue={80} data-width={90} data-height={90} data-thickness="0.1" data-fgcolor="#21ba45" />
                                        </div>
                                    </div>
                                </div>
                           </div>
                           <div className="row clearfix mt-2">
                                <div className='tab-content'>
                                        <ul className="list-group">
                                    {this.state.questions.map((result, i) =>(
                                        <li key={i} className="mb-6">
                                            <p><strong>{result.wording}</strong></p>
                                            <p className={result.selected === result.response ? 'list-group-item list-group-item-success' : 
                                            'list-group-item list-group-item-danger'}><b>réponse du candidat:</b> {result.selected}</p>
                                            {result.selected !== result.response  && <p className='ist-group-item list-group-item-secondary'><b>réponse correcte:</b> {result.response}</p>}
                                        </li>
                                    ))}
                                    <li>
                                    <p><strong>Déclarer un Hook dans cette classe</strong></p></li>
                                    <ReactPlayer url='assets/video1.MP4' controls = {true}/>
                                    <li>
                                    <p><strong>Modifier ce programme afin  d'indiquer à Spring que la classe doit être détectée en tant que contrôleur</strong></p></li>
                                    <ReactPlayer url='assets/video2.MP4' controls = {true}/>
                                     </ul>

                                </div>
                                </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
    </div>
            </React.Fragment>
        )
    }
}

export default Evaluationreport
