import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Login from './components/Login'
import Home from './components/Home'
import EnvoieEvaluation from './components/InfosApplicant';
import EmailView from './components/EmailView'
import Form from './components/Form'
import Evaluationreport from './components/Evaluationreport'
import App from './App'
import ScreenRecording from './components/ScreenRecording'

ReactDOM.render(
 <BrowserRouter>
  <Switch>
      <Route exact path='/' component={Login} />
      <Route path='/home/:firstname' component={Home} />
      <Route path='/home' component={Home} />
      <Route path='/envoieEvaluation' component={EnvoieEvaluation} />
      <Route path='/emailview' component={EmailView} />
      <Route path='/creation_of_campaign' component={Form}/>
      <Route path='/eval6321893' component={App} />
      <Route path='/reportEval6321893' component={Evaluationreport} />
      <Route path='/recording' component={ScreenRecording} />

  </Switch>
 </BrowserRouter>,
  document.getElementById('root')
);

reportWebVitals();
