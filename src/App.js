import './App.css';
import {useState, useEffect} from 'react';
import Start from './components/Start';
import Question from './components/Question';
import End from './components/End'
import getItemsQuestion from './services/ItemsQuestion';

let interval
// Main component of app(load other ones)
const App = () => {
  //in order to know the loaded component
  const [step, setStep] = useState(1);
  // in order to know which question is selected 
  const [activeQuestion, setActiveQuestion] = useState(0)
  //questions and answers
  const [itemsQuestionAnswer,setItemsQuestionAnswer] = useState([])
  // answers of questions
  const [answers, setAnswers] = useState([])
  //to manage time
  const [time,setTime] = useState(0)

  useEffect(() => {
    getItemsByQuestion()
  },[]);

  const testStartHandler = () => {
    setStep(2)
    interval = setInterval(() => {
      setTime(prevTime => prevTime + 1)
    }, 1000);
  }

  const getItemsByQuestion = () => {
    getItemsQuestion().then(response => {
      setItemsQuestionAnswer({itemsQuestionAnswer: response.data})
    });
  }
  return (
    <div className=''>
      {step === 1 && <Start onTestStart={testStartHandler}/>}
      {step === 2 && <Question 
        data={itemsQuestionAnswer[activeQuestion]}
        onAnswerUpdate={setAnswers}
        numberOfQuestions={itemsQuestionAnswer.length}
        activeQuestion={activeQuestion}
        onSetActiveQuestion={setActiveQuestion}
        onSetstep={setStep}
        answers={answers}
        interval={interval}
      />}
      {step === 3 && <End time={time} />}
    </div>
  );
}
export default App;
