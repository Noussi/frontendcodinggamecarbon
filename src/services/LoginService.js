// service to get evaluators from database
import axios from "axios";
const POST_API_BASE_URL_EVALUATOR = 'http://localhost:9090/evaluators';

export default function getEvaluators() {
  return axios.get(POST_API_BASE_URL_EVALUATOR);
}