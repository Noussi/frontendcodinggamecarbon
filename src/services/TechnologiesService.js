// service to get technologies from database
import axios from "axios"
const POST_API_BASE_URL_Technologies='http://localhost:9090/technology'

export default function getTechnologies(){
    return axios.get(POST_API_BASE_URL_Technologies);
}