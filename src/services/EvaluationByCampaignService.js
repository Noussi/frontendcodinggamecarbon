// service to get Evaluations by campaign from database
import axios from "axios";

export default function getEvaluationsByCampaign(id) {
  return axios.get('http://localhost:9090/evaluations/'+ id);
}