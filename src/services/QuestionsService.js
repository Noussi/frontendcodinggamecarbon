// service to get questions from database
import axios from "axios"
const POST_API_BASE_URL_Question='http://localhost:9090/questions'

export default function getQuestions(){
    return axios.get(POST_API_BASE_URL_Question);
}