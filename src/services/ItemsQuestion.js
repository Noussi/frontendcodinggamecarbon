// service to get items of questions from database
import axios from "axios"
const POST_API_BASE_URL_ItemsQuestion='http://localhost:9090/itemQuestions'

export default function getItemsQuestion(){
    return axios.get(POST_API_BASE_URL_ItemsQuestion);
}