// service to get campaign from database

import axios from "axios";
const POST_API_BASE_URL_Campaigns = 'http://localhost:9090/campaigns';

export default function getCampaigns() {
  return axios.get(POST_API_BASE_URL_Campaigns);
}
