// service to get Evaluations from database
import axios from "axios";
const POST_API_BASE_URL_EVALUATION = 'http://localhost:9090/evaluations';

export default function getEvaluations() {
  return axios.get(POST_API_BASE_URL_EVALUATION);
}