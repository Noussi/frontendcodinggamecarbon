// service to get answers selected from database
import axios from "axios"
const POST_API_BASE_URL_RESPONSE='http://localhost:9090/responses'

export default function getResp(){
    return axios.get(POST_API_BASE_URL_RESPONSE);
}